package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class WebDriverHelper {

    static WebDriver webDriver = null;

    private static ReentrantLock webDriverLock = new ReentrantLock();
    
    public static void getWebDriverLock() {
        if (!webDriverLock.isHeldByCurrentThread()) {
            webDriverLock.lock();
        }
    }

    public static void releaseWebDriverLock() {
        for (int i = 0; i < webDriverLock.getHoldCount(); i++) {
            webDriverLock.unlock();
        }

        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
        }
    }

    public static WebDriver getWebDriver(){

        getWebDriverLock();

        if (webDriver == null){
            System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");

            webDriver = new ChromeDriver(options);
            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

        }

        return webDriver;
    }

    public static void releaseWebDriver() {
        releaseWebDriverLock();
    }

    public static void closeWebDriver(){
        if (getWebDriver() !=  null){
            webDriver.close();
        }
    }

    public static void implicitlyWait(int millis){
        try {
            Thread.currentThread().sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    public static WebElement waitUntilElementVisibility(WebDriver driver, String cssSelector){
        WebDriverWait wait = new WebDriverWait(driver, 20);

        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
        }catch (TimeoutException e){

        }
        return null;
    }

    public static void setFocusOnElement(WebDriver driver, WebElement element){
        new Actions(driver).moveToElement(element).perform();
    }

    public static void drugElementTo(WebDriver driver, WebElement element, int xOffset, int yOffset){
        Actions builder = new Actions(driver);
        Action dragAndDrop = builder.dragAndDropBy(element, xOffset, yOffset) .build();
        dragAndDrop.perform();
    }



}
