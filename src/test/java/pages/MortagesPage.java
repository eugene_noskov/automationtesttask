package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WebDriverHelper;
import java.util.List;

public class MortagesPage {

    private final WebDriver driver;

    public MortagesPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(css="a[href='/mortgage-payment-calculator']")
    List<WebElement> mortagesMenuItem;

    public CalculatorPage transferToCalculatorPage(){

        WebElement element = mortagesMenuItem.get(mortagesMenuItem.size() - 1);
        WebDriverHelper.setFocusOnElement(driver, element);
        element.click();

        return PageFactory.initElements(driver, CalculatorPage.class);
    }
}
