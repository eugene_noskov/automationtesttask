package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WebDriverHelper;

import java.util.List;

public class CalculatorPage {

    private final WebDriver driver;

    public CalculatorPage(WebDriver driver) {
        this.driver = driver;
        WebDriverHelper.waitUntilElementVisibility(driver, "#form_calculateur_versements");
    }

    @FindBy(css="#PrixPropriete")
    WebElement purchasePriceInputField;


    public int getPurchasePrice() {
        String price = purchasePriceInputField.getAttribute("value").trim();
        return Integer.parseInt(price);
    }


    @FindBy(css="#form_calculateur_versements div.inputSlide-ia")
    List<WebElement> slidersHolders;

    public void moverPurchasePriceSliderMostRight(){
        WebElement slider = slidersHolders.get(0).findElement(By.cssSelector("div.slider-handle.min-slider-handle.custom"));
        WebDriverHelper.setFocusOnElement(driver, slider);
        WebDriverHelper.drugElementTo(driver, slider, 800, 0);

    }

    public void moverPurchasePriceSliderMostLeft() {
        WebElement slider = slidersHolders.get(0).findElement(By.cssSelector("div.slider-handle.min-slider-handle.custom"));
        WebDriverHelper.setFocusOnElement(driver, slider);
        WebDriverHelper.drugElementTo(driver, slider, -800, 0);
    }

    @FindBy(css="#PrixProprietePlus")
    WebElement purchasePriceBtnPlus;
    public void clickOnPurchasePlusButton() {
        purchasePriceBtnPlus.click();
    }


    @FindBy(css="#MiseDeFond")
    WebElement downPaymenSumField;
    public void setDownPaymentToSum(String sum){
        WebDriverHelper.setFocusOnElement(driver, downPaymenSumField);
        WebDriverHelper.implicitlyWait(500);

        downPaymenSumField.clear();

        downPaymenSumField.sendKeys(sum);

        downPaymenSumField.sendKeys(Keys.RETURN);
    }


    @FindBy(css="div.selectric-wrapper")
    List<WebElement> selectInputFildsField;
    public void setAmortizationToYears(String numYears){

        selectInputFildsField.get(0).click();

        WebDriverHelper.setFocusOnElement(driver, selectInputFildsField.get(0));

        List<WebElement> amortItems = selectInputFildsField.get(0).findElements(By.cssSelector("li"));

        for (WebElement item : amortItems) {
            if(item.getText().trim().toLowerCase().startsWith(numYears)){
                item.click();
                break;
            }
        }
    }

    public void setPaymentFrequency(String frequency){
        selectInputFildsField.get(1).click();

        WebDriverHelper.setFocusOnElement(driver, selectInputFildsField.get(1));

        List<WebElement> amortItems = selectInputFildsField.get(1).findElements(By.cssSelector("li"));

        for (WebElement item : amortItems) {
            if(item.getText().trim().equalsIgnoreCase(frequency)){
                item.click();
                break;
            }
        }
    }

    @FindBy(css="#TauxInteret")
    WebElement interestRateInputField;

    public void setInterestRateTo(String rate) {
        WebDriverHelper.setFocusOnElement(driver, interestRateInputField);

        interestRateInputField.clear();
        interestRateInputField.sendKeys(rate);
        interestRateInputField.sendKeys(Keys.RETURN);
    }

    @FindBy(css="#btn_calculer")
    WebElement calculateButton;

    public void clickCalculateButton() {
        calculateButton.click();
        WebDriverHelper.implicitlyWait(3000);
    }



}
