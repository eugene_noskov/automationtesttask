package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private final WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(css="#nav-secondaire li.Pret")
    WebElement loansMenuGroup;
    @FindBy(css="section[aria-labelledby='Produits'] a[data-utag-name='mortgage_loan']")
    WebElement mortagesMenuItem;

    public MortagesPage clickLoans(){
        new Actions(driver).moveToElement(loansMenuGroup).perform();
        loansMenuGroup.click();
        mortagesMenuItem.click();

        return PageFactory.initElements(driver, MortagesPage.class);
    }
}
