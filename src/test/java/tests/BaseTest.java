package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CalculatorPage;
import pages.HomePage;
import pages.MortagesPage;
import utils.WebDriverHelper;

import java.math.BigDecimal;

public class BaseTest {

    /**
     * Пояснения к реализации
     * 1. Проверки делал только те, которые указаны на странице теста. Если нужны будут еще какие-то дополнительные
     *  (типа присутсвие/отсутсивие), могу переделать.
     *
     * 2. Тестовой страницы менялась, видимо, слайдером цену покупки из задания установить нельзя. Решил чере установку в поле.
     *
     * 3. Не совсем понятны требования "репорт повинен бути прийнятий для клієнта",
     *      т.к. пока не сталкивался, в каком виде принято поставлять отчеты клиентам у вас в компании.
     *      Взял из примеров в интернете. Формирую два репорта, один стандартный, другой на основе ExtentReports
     *
     * 4. Для инициализации объектов страниц вызываю PageFactory.initElements(driver, СlassName.class).
     *      Посчитал, что это считается за использование шаблона Factory. Если нужно, переделаю, создам свою фабрику.
     * */


    public static WebDriver driver;

    CalculatorPage calculatorPage;

    @BeforeClass
    public void initializeWebDriver(){
        driver = WebDriverHelper.getWebDriver();
    }

    @Test(description = "Opening mortgage payment calculator page")
    public void openCalculatorPage() {
        BaseTest.driver.get("https://ia.ca/individuals");

        HomePage page = PageFactory.initElements(driver, HomePage.class);
        MortagesPage mPage = page.clickLoans();

        calculatorPage = mPage.transferToCalculatorPage();
    }

    @Test(priority = 1, dependsOnMethods = "openCalculatorPage", description = "Purchase price slider working test")
    public void PurchasePriceSliderTest() {

        calculatorPage.moverPurchasePriceSliderMostRight();

        Assert.assertEquals(2000000, calculatorPage.getPurchasePrice(), "In the most right slider position expecting purchase sum is 2000000");

    }

    @Test(priority = 2, dependsOnMethods = "openCalculatorPage", description = "Filling calculator fields with testing data")
    public void settingUpCalculator() {

        calculatorPage.moverPurchasePriceSliderMostLeft();

        //Clicking on Purchase + button
        for (int i = 0; i < 2; i++) {
            calculatorPage.clickOnPurchasePlusButton();
        }

        //В задании написано, что нужно нажимать на плюс, чтобы добиться суммы
        //но сайт видимо изменился, и нету больше такой градации, потоэтому сделал
        //ввод суммы через поле
        calculatorPage.setDownPaymentToSum("50000");

        calculatorPage.setAmortizationToYears("15");

        calculatorPage.setPaymentFrequency("weekly");

        calculatorPage.setInterestRateTo("5");

        calculatorPage.clickCalculateButton();


    }

    @Test(dependsOnMethods = "settingUpCalculator", description = "Checking calculated sum")
    public void calculationSumTest() {
        // - проверить сумму $842.47 в задании 836.75
        WebElement sumElement = driver.findElement(By.cssSelector("#paiement-resultats"));
        String valStr = sumElement.getText().replaceAll("(\\s|\\$)", "");

        Assert.assertEquals(new BigDecimal("836.75"), new BigDecimal(valStr), "Wrong calculation result: ");
    }

    @AfterClass
    public void finishTests(){
        WebDriverHelper.releaseWebDriver();
    }

    @AfterSuite
    public void releaseResources(){
        WebDriverHelper.closeWebDriver();
    }
}
